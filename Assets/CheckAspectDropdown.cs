﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckAspectDropdown : MonoBehaviour {

    public Dropdown aspectDropdown;
	// Use this for initialization
	void Start () {
		if (aspectDropdown.value == 0 && gameObject.name.Contains("Centre"))
        {
            GetComponent<Toggle>().interactable = false;
        }
        else
        {
            GetComponent<Toggle>().interactable = true;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
