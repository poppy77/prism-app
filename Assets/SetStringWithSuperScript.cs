﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetStringWithSuperScript : MonoBehaviour {

    public string text;

	// Use this for initialization
	void Start () {
        text = text.Replace("(ss2)", "\xB2");
        GetComponent<Text>().text = text;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
