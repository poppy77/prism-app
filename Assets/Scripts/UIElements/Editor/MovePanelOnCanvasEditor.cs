﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BrydenWoodUnity.UIElements
{
    [CustomEditor(typeof(MovePanelOnCanvas))]
    public class MovePanelOnCanvasEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            var myTarget = (MovePanelOnCanvas)target;
            if (GUILayout.Button("Set On Location"))
            {
                myTarget.onPosition = myTarget.GetComponent<RectTransform>().anchoredPosition;
            }
            if (GUILayout.Button("Set Off Location"))
            {
                myTarget.offPosition = myTarget.GetComponent<RectTransform>().anchoredPosition;
            }
        }
    }
}
