﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BrydenWoodUnity.UIElements
{
    [CustomEditor(typeof(GlobalTooltip))]
    public class GlobalTooltipEditor : Editor
    {
        SerializedProperty tooltipMessage;
        SerializedProperty warningMessage;
        Vector2 scroll1;
        Vector2 scroll2;

        void OnEnable()
        {
            tooltipMessage = serializedObject.FindProperty("tooltipMessage");
            warningMessage = serializedObject.FindProperty("warningMessage");
        }

        public override void OnInspectorGUI()
        {
            var globalTooltip = (target as GlobalTooltip);
            serializedObject.Update();
            globalTooltip.delay = EditorGUILayout.FloatField("Delay (ms)", globalTooltip.delay);
            EditorGUILayout.LabelField("Tooltip Message:");
            scroll1 = EditorGUILayout.BeginScrollView(scroll1, GUILayout.MaxHeight(75));
            tooltipMessage.stringValue = EditorGUILayout.TextArea(tooltipMessage.stringValue ,GUILayout.MaxHeight(75));
            EditorGUILayout.EndScrollView();
            EditorGUILayout.LabelField("Tooltip Message:");
            scroll2 = EditorGUILayout.BeginScrollView(scroll1, GUILayout.MaxHeight(75));
            warningMessage.stringValue = EditorGUILayout.TextArea(warningMessage.stringValue, GUILayout.MaxHeight(75));
            EditorGUILayout.EndScrollView();
            serializedObject.ApplyModifiedProperties();
        }
    }
}
