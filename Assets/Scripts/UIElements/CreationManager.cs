﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.UIElements;
using System.Linq;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component to manage the Creation Panels in the Canvas
    /// </summary>
    public class CreationManager : MonoBehaviour
    {
        #region Public Fields and Properties
        public Toggle[] creationModes;
        public MovePanelOnCanvas[] creationPanels;
        public ToggleGroup creationTogGroup;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            //state = false;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Detects which of the creation panels is active
        /// </summary>
        /// <param name="state">The state of the panel which are not active</param>
        public void DetectActiveMode(bool state)
        {
            //state = !state;
            for (int i = 0; i < creationModes.Length; i++)
            {
                if (creationModes[i].isOn)
                {
                    creationPanels[i].OnToggle(state);
                }
            }
        }
        #endregion
    }
}
