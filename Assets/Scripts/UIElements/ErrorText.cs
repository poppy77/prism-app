﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for displaying error messages
    /// </summary>
    public class ErrorText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
    {
        #region Public Fields and Properties
        public bool clicked = false;
        #endregion

        #region Events
        /// <summary>
        /// Used when the error message is hover-over
        /// </summary>
        /// <param name="sender">The message which is hover-over</param>
        /// <param name="show">Whether the mouse is over or not</param>
        public delegate void OnHoverOverError(Text sender, bool show);
        /// <summary>
        /// Triggered when the error message is hover-over
        /// </summary>
        public static event OnHoverOverError hoverOver;
        /// <summary>
        /// Triggered when the error message is clicked
        /// </summary>
        public static event OnHoverOverError click;
        #endregion

        #region MonoBehaviour Methods
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (hoverOver != null)
            {
                hoverOver(GetComponent<Text>(), true);
            }
            if (clicked)
            {
                clicked = false;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!clicked)
            {
                if (hoverOver != null)
                {
                    hoverOver(GetComponent<Text>(), false);
                }
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (click != null)
            {
                click(GetComponent<Text>(), false);
            }
            clicked = true;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion
    }
}
