﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.Lighting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A base MonoBehaviour component for UI representation of scene objects
    /// </summary>
    public class UiRepresentation : MonoBehaviour
    {
        #region Public Fields and Properties
        /// <summary>
        /// The gameObject in scene to be moved by this control point
        /// </summary>
        [Tooltip("The gameObject in scene to be moved by this control point")]
        public List<Transform> sceneObjects;
        #endregion

        #region Private Fields and Properties
        protected Vector3 prevCameraPosition;
        protected bool hasInitialized = false;
        protected float prevOrthoSize;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            if (sceneObjects != null && sceneObjects.Count > 0)
            {
                Initialize(sceneObjects[0]);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (prevCameraPosition != Camera.main.transform.position || prevOrthoSize != Camera.main.orthographicSize)
            {
                UpdateUIPosition();
            }
            prevCameraPosition = Camera.main.transform.position;
            prevOrthoSize = Camera.main.orthographicSize;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Destroys the UI representation of the scene object
        /// </summary>
        /// <param name="sceneObject">The sceneObject of this representation</param>
        public virtual void DestroyUIRep(Transform sceneObject)
        {
            if (sceneObjects.Count == 1)
            {
                Destroy(gameObject);
            }
            else
            {
                sceneObjects.Remove(sceneObject);
            }
        }

        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="singleObject">The scene object of this representation</param>
        public virtual void Initialize(Transform singleObject)
        {
            if (sceneObjects == null || (sceneObjects.Count > 0 && sceneObjects[0] == null))
            {
                sceneObjects = new List<Transform>();
            }
            if (!sceneObjects.Contains(singleObject))
                sceneObjects.Add(singleObject);
            hasInitialized = true;
        }

        /// <summary>
        /// Adds a transform to the scene objects this representation controls
        /// </summary>
        /// <param name="transform">The transform to be added</param>
        public virtual void AddSceneObject(Transform transform)
        {
            if (sceneObjects == null) sceneObjects = new List<Transform>();

            sceneObjects.Add(transform);
        }

        /// <summary>
        /// Updates the canvas position of this representation
        /// </summary>
        public virtual void UpdateUIPosition()
        {
            if (hasInitialized)
            {
                if (sceneObjects.Count > 0 && sceneObjects[0] != null)
                {
                    GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(sceneObjects[0].position);
                }
            }
        }
        #endregion
    }
}
