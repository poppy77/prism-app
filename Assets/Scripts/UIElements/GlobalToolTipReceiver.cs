﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    public class GlobalToolTipReceiver : Tagged<GlobalToolTipReceiver>
    {
        public Text tooltipText;
        public RectTransform rectTransform;
        public float timeActive = 5000f;
        private Stopwatch stopwatch = new Stopwatch();
        private void OnEnable()
        {
            stopwatch.Reset();
            stopwatch.Start();
        }

        private void Update()
        {
            if (stopwatch.ElapsedMilliseconds>timeActive)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
