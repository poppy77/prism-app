﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BrydenWoodUnity.UIElements
{
    public class GlobalTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler

    {
        public string tooltipMessage;
        public string warningMessage;
        public float delay = 500f;
        private GlobalToolTipReceiver globalTooltip;
        private bool entered;
        private DateTime start;
        private bool display;
        private void Update()
        {
            if (entered)
            {
                if ((DateTime.Now - start).TotalMilliseconds > delay && display)
                {
                    var relativePos = 1;
                    if (Input.mousePosition.x > Screen.width / 2.0f)
                    {
                        relativePos = -1;
                    }
                    else
                    {
                        relativePos = 1;
                    }

                    if (GetComponent<Selectable>().interactable)
                    {
                        globalTooltip.tooltipText.color = Color.black;
                        globalTooltip.tooltipText.text = tooltipMessage;
                        UpdateInfoHeight();
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(warningMessage))
                        {
                            globalTooltip.tooltipText.color = Color.red;
                            globalTooltip.tooltipText.text = warningMessage;
                            UpdateInfoHeight();
                        }
                        else
                        {
                            globalTooltip.tooltipText.color = Color.black;
                            globalTooltip.tooltipText.text = tooltipMessage;
                            UpdateInfoHeight();
                        }
                    }
                    var deltaSize = globalTooltip.rectTransform.sizeDelta;
                    globalTooltip.rectTransform.position = Input.mousePosition + new Vector3((deltaSize.x / 2.0f) * relativePos, 0, -deltaSize.y / 2.0f);
                    globalTooltip.gameObject.SetActive(true);
                    display = false;
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!entered)
            {
                entered = true;
                start = DateTime.Now;
                display = true;
            }
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            globalTooltip.tooltipText.color = Color.black;
            globalTooltip.tooltipText.text = String.Empty;
            globalTooltip.gameObject.SetActive(false);
            entered = false;
        }

        private void Start()
        {
            globalTooltip = GlobalToolTipReceiver.TaggedObject;
        }

        private void UpdateInfoHeight()
        {
            float lineHeight = 16;// (globalTooltip.transform.root.localScale.y);
            var lines = globalTooltip.tooltipText.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            int linesNum = lines.Length + 1;
            float longestLine = float.MinValue;
            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Length > longestLine) longestLine = lines[i].Length;
            }
            globalTooltip.rectTransform.sizeDelta = new Vector2((lineHeight / 1.7f) * longestLine, lineHeight * linesNum);
        }

    }
}
