﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for manipulating the area range of an apartment type (UI)
    /// </summary>
    public class ApartmentAreaRange : MonoBehaviour, ICancelable
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public InputField areaRange;

        [Header("Range Information:")]
        public string type;
        public float originalMinArea;
        public float originalMaxArea;
        public float currentMinArea;
        public float currentMaxArea;
        #endregion

        private string prevValue;

        #region Events
        /// <summary>
        /// Used when the area range has changed
        /// </summary>
        public delegate void OnAreaChanged();
        /// <summary>
        /// Triggered when the area range has changed
        /// </summary>
        public static event OnAreaChanged areaChanged;
        #endregion

        #region MonoBehaviour Methods
        private void OnDestroy()
        {
            if (areaChanged != null)
            {
                foreach (Delegate eh in areaChanged.GetInvocationList())
                {
                    areaChanged -= (OnAreaChanged)eh;
                }
            }
        }

        // Use this for initialization
        private void Awake()
        {
            ProceduralBuildingManager.updateAreaRange += UpdateAreaRange;
        }
        void Start()
        {
            UpdateAreaRange();
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Resets the range to the initial values
        /// </summary>
        public void ResetRange()
        {
            areaRange.text = string.Format("{0}-{1}", originalMinArea, originalMaxArea);
            OnRangeChanged(string.Format("{0}-{1}", originalMinArea, originalMaxArea));
        }

        /// <summary>
        /// Called when the range has changed
        /// </summary>
        /// <param name="value">The new range as a string</param>
        public void OnRangeChanged(string value)
        {
            RefreshPopUp.TaggedObject.AddChangedValue(this, new string[] { prevValue, value });
        }

        /// <summary>
        /// Called when the apartments' area ranges have changed
        /// </summary>
        /// <param name="sender">The Standards class which logged the change</param>
        /// <param name="redraw">Whether the apartment layout should be re-drawn</param>
        public void OnStandardsAreasChanged(Standards sender, bool redraw)
        {
            UpdateAreaRange();
        }

        /// <summary>
        /// Updates the area range from the Standards class
        /// </summary>
        public void UpdateAreaRange()
        {
            if (Standards.TaggedObject.ApartmentTypesMaximumSizes.ContainsKey(type))
            {
                //originalMinArea = (float)Standards.TaggedObject.ApartmentTypesMinimumSizes[type];
                //originalMaxArea = (float)Standards.TaggedObject.ApartmentTypesMaximumSizes[type];
                areaRange.SetValue(string.Format("{0}-{1}", (float)Math.Round(Standards.TaggedObject.ApartmentTypesMinimumSizes[type]), (float)Math.Round(Standards.TaggedObject.ApartmentTypesMaximumSizes[type])));
                prevValue = areaRange.text;
            }
        }

        /// <summary>
        /// Resets the value of the area range
        /// </summary>
        /// <param name="prevValue">The value to reset to</param>
        public void ResetValue(string prevValue)
        {
            areaRange.SetValue(prevValue);
        }

        /// <summary>
        /// Submits the new value for the area range
        /// </summary>
        /// <param name="value">The new value to be submitted</param>
        public void SubmitValue(string value)
        {
            string[] cells;
            if (value.Contains("-"))
            {
                cells = value.Split('-');
            }
            else
            {
                cells = new string[] { value, value };
            }
            areaRange.textComponent.color = Color.black;
            if (!float.TryParse(cells[0], out currentMinArea))
            {
                currentMinArea = originalMinArea;
            }
            if (!float.TryParse(cells[1], out currentMaxArea))
            {
                currentMaxArea = originalMaxArea;
            }

            if (currentMinArea < originalMinArea)
            {
                areaRange.textComponent.color = Color.red;
                areaRange.text = "MinArea Small!";
                return;
            }

            Standards.TaggedObject.TrySetMinimumArea(type, currentMinArea);

            Standards.TaggedObject.TrySetMaximumArea(type, currentMaxArea);

            if (areaChanged != null)
            {
                areaChanged();
            }
            prevValue = value;
        }
        #endregion
    }
}
