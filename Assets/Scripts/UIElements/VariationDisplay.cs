﻿using BrydenWoodUnity.DesignData;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BrydenWoodUnity.UIElements;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for displaying the variation of values
    /// </summary>
    public class VariationDisplay : MonoBehaviour
    {
        #region Public Fields and Properties
        public ProceduralBuildingManager buildingManager;
        public Text title;
        public ApartmentTypesChart apartmentTypesChart;
        public Text min;
        public Text max;
        public GameObject entry;
        public Image image;
        public Gradient variationGradient;
        public float distribution = 0.5f;
        public List<float> samples;
        #endregion

        #region Private Fields and Properties
        private List<ApartmentUnity> LastApts { get; set; }
        private string LastType { get; set; }
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Awake()
        {
            if (buildingManager!=null)
            {
                ProceduralBuildingManager.previewChanged += OnViewModeChanged;
                OnViewModeChanged(buildingManager.previewMode);
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Updates the variation that is displayed
        /// </summary>
        /// <param name="type">Apartment type to be displayed</param>
        /// <param name="apts">List of apartments of that type</param>
        public void UpdateVariationDisplay(string type, List<ApartmentUnity> apts)
        {
            LastApts = apts;
            LastType = type;
            if (!String.IsNullOrEmpty(LastType) && LastApts != null)
            {
                double min = Standards.TaggedObject.ApartmentTypesMinimumSizes[type];
                double max = Standards.TaggedObject.ApartmentTypesMaximumSizes[type];
                this.min.text = min.ToString();
                this.max.text = max.ToString();
                //
                List<float> areas = new List<float>();

                for (int i = 0; i < apts.Count; i++)
                {
                    areas.Add(apts[i].Area);
                }

                var mapped = GetSamplesMapped((float)min, (float)max, areas);

                StartCoroutine(SetSamples(mapped));
            }
        }

        public void OnViewModeChanged(PreviewMode previewMode)
        {
            switch(previewMode)
            {
                case PreviewMode.Buildings:
                    title.text = "Distribution by Type (m\xB2) - Project";
                    break;
                case PreviewMode.Floors:
                    if (buildingManager.proceduralBuildings.Count > 0)
                    {
                        title.text = "Distribution by Type (m\xB2) - " + buildingManager.proceduralBuildings[buildingManager.currentBuildingIndex].buildingName;
                    }
                    else
                    {
                        title.text = "Distribution by Type (m\xB2) - ";
                    }
                    break;
            }
        }

        public void UpdateVariationDisplay(string type, ProceduralApartmentLayout apt)
        {
            //LastApts = apt;
            //LastType = type;
            //if (!String.IsNullOrEmpty(LastType) && LastApts != null)
            //{
            double min = Standards.TaggedObject.ApartmentTypesMinimumSizes[type];
            double max = Standards.TaggedObject.ApartmentTypesMaximumSizes[type];
            this.min.text = min.ToString();
            this.max.text = max.ToString();
            List<float> areas = new List<float>();

            areas.Add(apt.NIA);

            var mapped = GetSamplesMapped((float)min, (float)max, areas);

            StartCoroutine(SetSamples(mapped));
            //}
        }

        public void OnStandardsAreasChanged(Standards sender, bool redraw)
        {
            UpdateVariationDisplay(LastType, LastApts);
        }
        #endregion

        #region Private Methods


        private IEnumerator SetSamples(List<float> samples)
        {
            if (image.transform.childCount > 0)
            {
                for (int i = 0; i < image.transform.childCount; i++)
                {
                    Destroy(image.transform.GetChild(i).gameObject);
                }
            }
            yield return new WaitForEndOfFrame();
            this.samples = new List<float>();

            for (int i = 0; i < samples.Count; i++)
            {
                this.samples.Add(samples[i]);
            }

            SetImageHeatMap();
        }


        private void SetImageHeatMap()
        {
            Rect rect = image.rectTransform.rect;
            Texture2D texture = new Texture2D((int)rect.width, (int)rect.height, TextureFormat.ARGB32, false);

            for (int i = 0; i < texture.width; i++)
            {
                //Color c = variationGradient.Evaluate((float)i / texture.width);
                var position = (float)i / texture.width;

                var eval = GetCummulativeDistance(samples, position);

                Color c = variationGradient.Evaluate(eval);

                for (int j = 0; j < texture.height; j++)
                {
                    texture.SetPixel(i, j, c);
                }
            }

            texture.Apply();
            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
            image.sprite = sprite;

            for (int i = 0; i < samples.Count; i++)
            {
                var gObj = Instantiate(entry, image.transform);
                gObj.GetComponent<RectTransform>().anchoredPosition = new Vector3(rect.width * samples[i], -5, 0);
                gObj.SetActive(true);
            }
        }

        private float GetCummulativeDistance(List<float> points, float position)
        {
            float cDist = 0;

            for (int i = 0; i < points.Count; i++)
            {
                float dist = Math.Abs(points[i] - position);
                dist = Gaussian(dist, 0, distribution);
                cDist += dist;
            }

            return cDist / (points.Count * Gaussian(0, 0, distribution));
        }

        private List<float> GetSamplesMapped(float min, float max, List<float> areas)
        {
            List<float> mapped = new List<float>();

            for (int i = 0; i < areas.Count; i++)
            {
                var val = (areas[i] - min) / (max - min);
                if (val < 0)
                    val = 0;
                if (val > 1)
                    val = 1;
                mapped.Add(val);
            }

            return mapped;
        }

        //from: http://csharphelper.com/blog/2015/09/draw-a-normal-distribution-curve-in-c/
        private float Gaussian(float x, float mean, float stddev)
        {
            float one_over_2pi = (float)(1.0 / (stddev * Math.Sqrt(2 * Math.PI)));
            float var = stddev * stddev;
            return (float)(one_over_2pi *
                Math.Exp(-(x - mean) * (x - mean) / (2 * var)));
        }
        #endregion
    }
}
