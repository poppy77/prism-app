﻿using BrydenWoodUnity.DesignData;
using BrydenWoodUnity.GeometryManipulation;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour component for managing the display of general information
    /// </summary>
    public class GeneralInfoDisplay : MonoBehaviour
    {
        #region Public Fields and Properties
        [Header("Scene References:")]
        public Text title;
        public Text info;
        public ProceduralBuildingManager buildingManager;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            buildingManager.apartmentLayoutChanged += OnApartmentLayoutChanged;
            buildingManager.overallUpdated += OnUpdateInfo;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the information needs to be updated
        /// </summary>
        public void OnUpdateInfo()
        {
            var infos = buildingManager.GetSelectedInfo();
            title.text = infos[0];
            info.text = infos[1];
            UpdateInfoHeight();
        }

        /// <summary>
        /// Called when the selected apartment layout has changed
        /// </summary>
        public void OnApartmentLayoutChanged()
        {
            var infos = buildingManager.GetApartmentLayoutInfo();
            title.text = infos[0];
            info.text = infos[1];
            UpdateInfoHeight();
        }
        #endregion

        private void UpdateInfoHeight()
        {
            float lineHeight = BrydenWoodUtils.CalculateLineHeight(info) / (info.transform.root.localScale.y);
            int linesNum = info.text.Split(new char[] { '\n' }/*, StringSplitOptions.RemoveEmptyEntries*/).Length + 1;
            info.GetComponent<RectTransform>().sizeDelta = new Vector2(info.GetComponent<RectTransform>().sizeDelta.x, lineHeight * linesNum);
            info.GetComponent<RectTransform>().parent.GetComponent<RectTransform>().sizeDelta = info.GetComponent<RectTransform>().sizeDelta;
        }
    }
}
