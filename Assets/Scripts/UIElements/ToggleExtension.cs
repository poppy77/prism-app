﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.UIElements
{
    /// <summary>
    /// A MonoBehaviour extension of the Toggle component
    /// </summary>
    [RequireComponent(typeof(Toggle))]
    public class ToggleExtension : MonoBehaviour
    {

        private Toggle m_toggle;

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {
            m_toggle = GetComponent<Toggle>();
            OnToggle(m_toggle.isOn);
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Called when the Toggle component value changes
        /// </summary>
        /// <param name="toggle">The value of the Toggle component</param>
        public void OnToggle(bool toggle)
        {
            if (toggle)
            {
                m_toggle.targetGraphic.color = m_toggle.colors.normalColor;
            }
            else
            {
                m_toggle.targetGraphic.color = m_toggle.colors.pressedColor;
            }
        }
        #endregion
    }
}
