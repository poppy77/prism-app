﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BrydenWoodUnity.GeometryManipulation;
using System;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A Monobehaviour component for the room
    /// </summary>
    public class RoomUnity : BaseDesignData, IConstrainable
    {
        #region Public Fields and Properties
        public int id;
        public string type;
        public float area;
        public float ratio;

        public string constrainError = "All is ok!";
        public bool error = false;
        #endregion

        #region MonoBehaviour Methods
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void OnDestroy()
        {
            polygon.updated.RemoveListener(OnPolygonUpdated);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="id">The index of the room</param>
        /// <param name="type">The type of the room</param>
        public void Initialize(int id, string type)
        {
            this.id = id;
            this.type = type;
        }

        /// <summary>
        /// Sets the polygon for the ouline of the room
        /// </summary>
        /// <param name="polygon">The outline of the room</param>
        public override void SetPolygon(Polygon polygon)
        {
            this.polygon = polygon;
            //polygon.updated += OnPolygonUpdated;
            polygon.updated.AddListener(OnPolygonUpdated);
            polygon.selected += Select;
            polygon.deselected += DeSelect;
            if (polygon.extrusion != null)
            {
                polygon.extrusion.selected += Select;
                polygon.extrusion.deselected += DeSelect;
                if (type == "Balcony")
                {
                    polygon.extrusion.totalHeight = 1.0f;
                    polygon.extrusion.GenerateMeshes();
                }
            }
            OnAreaChanged();
        }

        /// <summary>
        /// Sets the editable mesh for the outline of the room
        /// </summary>
        /// <param name="editableMesh">The outline of the room</param>
        public override void SetEditableMesh(EditableMesh editableMesh)
        {
            this.editableMesh = editableMesh;
            editableMesh.updated += OnEditableMeshUpdated;
            editableMesh.selected += Select;
            editableMesh.GetExtrusion();
            if (editableMesh.meshExtrusion != null)
            {
                editableMesh.meshExtrusion.selected += Select;
            }
            OnAreaChanged();
            Color m_color;
            if (Standards.TaggedObject.RoomTypesColors.TryGetValue(type, out m_color))
            {
                editableMesh.SetTypeColor(m_color);
            }
            else
            {
                editableMesh.SetTypeColor(Color.gray);
            }
        }

        /// <summary>
        /// Returns a notifcations regarding the errors of the apartment
        /// </summary>
        /// <returns>String</returns>
        public string GetNotification()
        {
            return constrainError;
        }

        /// <summary>
        /// Highlight the room if there is an error regarding its area
        /// </summary>
        /// <param name="show">Toggle the highlight</param>
        public void HighlightErrorObject(bool show)
        {
            if (!isSelected)
            {
                if (polygon != null)
                    polygon.ErrorHighlight(show);
                else if (editableMesh != null)
                    editableMesh.ErrorHighlight(show);
            }
        }

        /// <summary>
        /// Select this room if there is an error regarding its area
        /// </summary>
        public void ErrorSelect()
        {
            if (gameObject.activeSelf)
            {
                Select();
            }
        }

        /// <summary>
        /// Select the room
        /// </summary>
        public override void Select(bool triggerEvent = true)
        {
            if (polygon != null)
                polygon.SelectHighlight(true);
            else if (editableMesh != null)
                editableMesh.SelectHighlight(true);

            if (triggerEvent)
                OnSelected();
        }

        /// <summary>
        /// Deselect the room
        /// </summary>
        public override void DeSelect(bool triggerEvent = true)
        {
            if (polygon != null)
                polygon.SelectHighlight(false);
            else if (editableMesh != null)
                editableMesh.SelectHighlight(false);
            if (triggerEvent)
                OnDeSelected();
        }

        /// <summary>
        /// Returns information about the room
        /// </summary>
        /// <returns>String Array</returns>
        public override string[] GetInfoText()
        {
            string title = string.Format("{0} Info:\r\n", type);

            string info = "";
            info += string.Format("Room id: {0}\r\n", id);
            info += string.Format("Room area: {0}m\xB2\r\n", Math.Round(area));
            info += string.Format("Room ratio: 1/{0}\r\n", 1 / ratio);
            info += string.Format("Room orientation: {0}\r\n", "N/A");

            return new string[2] { title, info };
        }
        #endregion

        #region Private Properties
        protected override void OnPolygonUpdated(Polygon sender)
        {
            OnAreaChanged();
            CalculateRatio();
        }

        protected override void OnEditableMeshUpdated(EditableMesh mesh)
        {
            OnAreaChanged();
        }

        private void CalculateRatio()
        {
            var len1 = Vector3.Distance(polygon[0].currentPosition, polygon[1].currentPosition);
            var len2 = Vector3.Distance(polygon[1].currentPosition, polygon[2].currentPosition);
            if (len1 >= len2)
            {
                ratio = len2 / len1;
            }
            else
            {
                ratio = len1 / len2;
            }
        }

        private void OnAreaChanged()
        {
            area = polygon.Area;
            OnGeometryUpdated();
            //CheckAgainstConstraints();
        }

        private void CheckAgainstConstraints()
        {
            double minArea;
            if (Standards.TaggedObject.RoomTypesMinimumSizes.TryGetValue(type, out minArea))
            {
                if (area < minArea)
                {
                    constrainError = "The area of " + gameObject.name + " is too small!\r\nA room of type " + type + " should be at least " + Standards.TaggedObject.RoomTypesMinimumSizes[type] + "m\xB2";
                    error = true;
                }
                else
                {
                    constrainError = "All is ok!";
                    error = false;
                }
                base.ConstraintsChanged(this, error);
                HighlightErrorObject(error);
            }
        }
        #endregion
    }
}
