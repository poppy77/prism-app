﻿using BrydenWoodUnity.GeometryManipulation;
using BrydenWoodUnity.UIElements;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace BrydenWoodUnity.DesignData
{
    /// <summary>
    /// A MonoBehaviour component for a procedural tower
    /// </summary>
    public class ProceduralTower : ProceduralFloor
    {

        [Header("Tower Settings:")]
        public string configurationType;
        public Vector3 coreDimensions;

        public ProceduralTower baseTower;

        public FloorElement floorUIElement;

        public override float GIA
        {
            get
            {
                if (baseTower == null)
                {
                    var gia = 0.0f;
                    if (apartments != null)
                    {
                        for (int i = 0; i < apartments.Count; i++)
                        {
                            gia += apartments[i].editableMesh.area;
                        }
                    }
                    gia += (coreDimensions.x * coreDimensions.z);
                    return Mathf.Round(gia);
                }
                else if (baseTower != null)
                    return baseTower.GIA;
                else return 0;
            }
        }

        public override float NIA
        {
            get
            {
                float floorArea = 0.0f;

                if (apartments != null)
                {
                    for (int i = 0; i < apartments.Count; i++)
                    {
                        floorArea += apartments[i].NIA;
                    }
                }

                return Mathf.Round(floorArea);
            }
        }

        public string[] aptTypesList { get; private set; }

        #region Events
        /// <summary>
        /// Delegate for the event of the outter envelope changing
        /// </summary>
        /// <param name="sender">The sender of the event</param>
        public delegate void OutterEnvelopeChange(ProceduralTower sender);
        /// <summary>
        /// Triggered when the outter envelope changes
        /// </summary>
        public static event OutterEnvelopeChange OnOutterEnvelopeChanged;
        #endregion


        #region MonoBehaviour Methods
        private void Start()
        {
            waitFrame = new WaitForEndOfFrame();
        }

        private void Update()
        {

        }

        private void OnDestroy()
        {
            ProceduralTower.OnOutterEnvelopeChanged -= OnBaseTowerChanged;
            if (drawBuildingLines != null)
            {
                drawBuildingLines.RemoveBuildingLines(key);
                drawBuildingLines.RemovePlantRoomLines(key);
            }
        }
        #endregion


        #region Public Methods
        /// <summary>
        /// Initializes the Instance
        /// </summary>
        /// <param name="aptTypes">The list of apartment types</param>
        /// <param name="coreDimensions">The dimensions of the core</param>
        public void Initialize(string[] aptTypes, Vector3 coreDimensions)
        {
            aptTypesList = aptTypes;
            this.coreDimensions = new Vector3(coreDimensions.x, coreDimensions.y, coreDimensions.z);
            floor2floor = coreDimensions.y;
            //aptTypesList = new string[] { "2b4p", "2b4p", "1b2p", "2b4p", "2b4p", "1b2p" };//Standards.TaggedObject.towerConfigurations[configurationType].Split(',');
            drawBuildingLines = Camera.main.GetComponent<DrawBuildingLines>();
            percentages = FloorElement.GetTowerPercentages(aptTypesList);
            //GenerateGeometries();
        }

        /// <summary>
        /// Initializes the INstance
        /// </summary>
        /// <param name="tower">A tower to be used as a base for this one</param>
        public void Initialize(ProceduralTower tower)
        {
            baseTower = tower;
            configurationType = baseTower.configurationType;
            coreDimensions = baseTower.coreDimensions;
            drawBuildingLines = Camera.main.GetComponent<DrawBuildingLines>();
        }

        /// <summary>
        /// Populates the apartments of the floor layout with the internal layout
        /// </summary>
        /// <param name="show">Whether the internal layouts should be visible</param>
        /// <param name="repRootParent">The parent object for the internal layouts' representation</param>
        /// <param name="keepMesh">Whether the apartments should keep their external meshes</param>
        public override void PopulateApartments(bool show, Transform repRootParent, bool keepMesh = false)
        {
            if (transform.GetChild(0).gameObject.activeSelf)
            {
                if (levels[0] != 0)
                {
                    exteriorPolygon.gameObject.SetActive(!show);
                }
                GameObject thisFloorRepParent = new GameObject("Floor Rep_" + gameObject.name);
                thisFloorRepParent.transform.SetParent(repRootParent);
                for (int i = 0; i < apartments.Count; i++)
                {
                    if (apartments[i].ApartmentType != "SpecialApt")
                    {
                        if (show)
                        {
                            if (apartments[i].Representation == null)
                            {
                                apartments[i].PopulateInternalLayout(proceduralApartmentLayout, apartmentDepth, thisFloorRepParent.transform, keepMesh);
                            }
                            else
                            {
                                apartments[i].ToggleRepresentation(show);
                            }
                        }
                        else
                        {
                            apartments[i].ToggleRepresentation(show);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Updates the floors outlines
        /// </summary>
        /// <param name="mode">The current preview mode</param>
        public override void UpdateFloorOutline(PreviewMode mode)
        {
            List<Line> lines = new List<Line>();
            float offsetDist = Standards.TaggedObject.ConstructionFeatures["ExteriorWall"] + 0.1f;
            switch (mode)
            {
                case PreviewMode.Floors:
                    for (int i = 0; i < exteriorPolygon.Count; i++)
                    {
                        int next = (i + 1) % exteriorPolygon.Count;
                        lines.Add(new Line(exteriorPolygon[i].currentPosition, exteriorPolygon[next].currentPosition, Color.black, Vector3.up, 0.1f));
                    }
                    break;
                case PreviewMode.Buildings:
                    for (int m = 0; m < levels.Length; m++)
                    {
                        Vector3 addHeight = new Vector3(0, m * floor2floor, 0);
                        if (exteriorPolygon != null)
                        {
                            for (int i = 0; i < exteriorPolygon.Count; i++)
                            {
                                int next = (i + 1) % exteriorPolygon.Count;
                                lines.Add(new Line(exteriorPolygon[i].currentPosition + addHeight, exteriorPolygon[next].currentPosition + addHeight, Color.black, Vector3.up, 0.1f));
                            }
                        }
                    }
                    break;
            }

            if (levels[0] == 0)
            {
                List<Line> plantRoomLines = new List<Line>();
                if (exteriorPolygon != null)
                {
                    for (int i = 0; i < exteriorPolygon.Count; i++)
                    {
                        int next = (i + 1) % exteriorPolygon.Count;
                        lines.Add(new Line(exteriorPolygon[i].currentPosition, exteriorPolygon[next].currentPosition, plantRoomColor, Vector3.down, 0.1f));
                    }
                }
                drawBuildingLines.AddPlantRoomLines(new KeyValuePair<string, List<Line>>(this.key, plantRoomLines));
            }
            drawBuildingLines.AddBuildingLines(new KeyValuePair<string, List<Line>>(this.key, lines));
        }

        /// <summary>
        /// Sets the levels of this floor
        /// </summary>
        /// <param name="levels">The new levels</param>
        public override void SetLevels(int[] levels)
        {
            if (levels[0] == 0)
            {
                Material commercialMaterial = Resources.Load("Materials/Commercial") as Material;
                exteriorPolygon.SetOriginalMaterial(commercialMaterial);
            }
        }

        /// <summary>
        /// Updates the floor to floor height and levels information of the floor layout
        /// </summary>
        /// <param name="f2f">Floor to floor height</param>
        /// <param name="levels">The levels as a list</param>
        public override void UpdateLevels(float f2f, int[] levels)
        {
            this.levels = levels;
            floor2floor = f2f;
            if (hasGeometry)
            {
                this.levels = levels;
                floor2floor = f2f;
                GenerateExteriorPolygon();
                UpdateFloorOutline(building.buildingManager.previewMode);
                SetPreviewMode(building.buildingManager.previewMode);
                building.OnFloorHeightChanged();
            }
        }

        /// <summary>
        /// Sets the list of apartment types for this tower floor
        /// </summary>
        /// <param name="types">The list of apartment types</param>
        /// <param name="percentages">The percentages for this tower floor</param>
        public void SetApartmentTypes(string[] types, Dictionary<string, float> percentages)
        {
            aptTypesList = types;
            this.percentages = percentages;
        }

        /// <summary>
        /// Generates the geometries of the floor and of the apartments
        /// </summary>
        public void GenerateGeometries()
        {
            GenerateGeometries(building.buildingManager.previewMode);
        }

        /// <summary>
        /// Generates the geometries of the floor and of the apartments
        /// </summary>
        /// <param name="mode">A specific preview mode to be set after generation</param>
        public void GenerateGeometries(PreviewMode mode)
        {
            if (baseTower == null)
            {
                var yVal = building.buildingElement.GetYForLevel(building.floors.IndexOf(this));
                transform.position = new Vector3(transform.position.x, yVal, transform.position.z);
                GetCore();
                GetApartments();
                GenerateExteriorPolygon();
                hasGeometry = true;
                for (int i = 0; i < apartments.Count; i++)
                {
                    apartments[i].geometryUpdated.AddListener(OnApartmentUpdated);
                }
            }
            else
            {
                apartments = new List<ApartmentUnity>();
                GetCore();
                GenerateExteriorPolygon(baseTower);
                GeneratePlantRoom(building.GetBuildingPlantArea());
                hasGeometry = true;
                ProceduralTower.OnOutterEnvelopeChanged += OnBaseTowerChanged;
            }
            SetPreviewMode(mode);
        }

        /// <summary>
        /// Generates the plant room for this floor layout
        /// </summary>
        /// <param name="area">The required area for the plant room</param>
        public override void GeneratePlantRoom(float area)
        {
            if (area > GIA)
            {
                plantRoomColor = Color.red;
                if (!Notifications.TaggedObject.activeNotifications.Contains("PlantRoom"))
                {
                    Notifications.TaggedObject.activeNotifications.Add("PlantRoom");
                    Notifications.TaggedObject.UpdateNotifications();
                }
            }
            else
            {
                plantRoomColor = Color.black;
                if (Notifications.TaggedObject.activeNotifications.Contains("PlantRoom"))
                {
                    Notifications.TaggedObject.activeNotifications.Remove("PlantRoom");
                    Notifications.TaggedObject.UpdateNotifications();
                }
            }
        }

        /// <summary>
        /// Returns the Floor Layout Save State
        /// </summary>
        /// <returns>FLoor Layout Save State</returns>
        public override FloorLayoutState GetLayoutState()
        {
            UpdateLayoutState();
            return saveState;
        }

        /// <summary>
        /// Updates the Floor Layout Save State
        /// </summary>
        public override void UpdateLayoutState()
        {
            saveState = new FloorLayoutState(building.index, id, false, false, apartmentDepth, corridorWidth, true, aptTypesList);
            saveState.centreLine = null;
            saveState.isCustom = false;
            saveState.customParams = null;
            saveState.apartmentStates = new List<ApartmentState>();
            saveState.isTower = true;
            saveState.position = new float[] { transform.position[0], transform.position[1], transform.position[2] };
            saveState.euler = new float[] { transform.eulerAngles[0], transform.eulerAngles[1], transform.eulerAngles[2] };

            for (int i = 0; i < apartments.Count; i++)
            {
                saveState.apartmentStates.Add(apartments[i].GetApartmentState());
            }
            saveState.minApartmentWidth = building.prevMinimumWidth;
        }

        /// <summary>
        /// Loads the geometries of this floor layout
        /// </summary>
        /// <param name="loadState">The load data for the layout</param>
        /// <param name="masterFloor">The base (ground floor) of the building</param>
        public override void LoadGeometries(FloorLayoutState loadState, ProceduralFloor masterFloor)
        {
            baseTower = masterFloor as ProceduralTower;

            if (baseTower == null)
            {
                yVal = loadState.position[1];
                transform.position = new Vector3(loadState.position[0], loadState.position[1], loadState.position[2]);
                GetCore();

                if (aptParent != null)
                {
                    DestroyImmediate(aptParent);
                }

                floorVertices = new List<GeometryVertex>();
                apartments = new List<ApartmentUnity>(6);
                aptParent = new GameObject("ApartmentsParent");
                aptParent.transform.SetParent(transform);
                aptParent.transform.localPosition = Vector3.zero;
                aptParent.transform.localEulerAngles = Vector3.zero;

                LoadApartments(loadState);
                GenerateExteriorPolygon();
                hasGeometry = true;
                for (int i = 0; i < apartments.Count; i++)
                {
                    apartments[i].geometryUpdated.AddListener(OnApartmentUpdated);
                }
            }
            else
            {
                apartments = new List<ApartmentUnity>();
                yVal = loadState.position[1];
                transform.position = new Vector3(loadState.position[0], loadState.position[1], loadState.position[2]);
                GetCore();
                GenerateExteriorPolygon(baseTower);
                GeneratePlantRoom(building.GetBuildingPlantArea());
                hasGeometry = true;
                ProceduralTower.OnOutterEnvelopeChanged += OnBaseTowerChanged;
            }
            building.Placed = true;
            SetPreviewMode(building.buildingManager.previewMode);
        }

        /// <summary>
        /// Generates the exterior polygon of the floor layout (outline of the external wall)
        /// </summary>
        public override void GenerateExteriorPolygon()
        {
            if (exteriorPolygon == null)
            {
                Vector3[] points = new Vector3[]
                {
                apartments[0].editableMesh[3].currentPosition,
                apartments[0].editableMesh[2].currentPosition,
                apartments[1].editableMesh[1].currentPosition,
                apartments[1].editableMesh[0].currentPosition,
                apartments[2].editableMesh[2].currentPosition,
                apartments[2].editableMesh[1].currentPosition,
                apartments[3].editableMesh[3].currentPosition,
                apartments[3].editableMesh[2].currentPosition,
                apartments[4].editableMesh[1].currentPosition,
                apartments[4].editableMesh[0].currentPosition,
                apartments[5].editableMesh[2].currentPosition,
                apartments[5].editableMesh[1].currentPosition,
                };
                //var pts = points.Distinct().ToArray();
                var offsetPoints = Polygon.OffsetPoints(points, true, -Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);

                exteriorPolygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                exteriorPolygon.gameObject.name = "ExteriorEnvelopePolygon";
                exteriorPolygon.GetComponent<MeshRenderer>().enabled = false;
                exteriorPolygon.transform.localPosition = Vector3.zero;
                //DestroyImmediate(exteriorPolygon.gameObject);
                //exteriorPolygon = null;
                List<PolygonVertex> verts = new List<PolygonVertex>();

                for (int i = 0; i < offsetPoints.Length; i++)
                {
                    PolygonVertex vert = Instantiate(polygonVertexPrefab, exteriorPolygon.transform).GetComponent<PolygonVertex>();
                    vert.Initialize(i);
                    vert.UpdatePosition(offsetPoints[i]);
                    verts.Add(vert);
                }

                exteriorPolygon.Initialize(verts, true, true, false);

                Extrusion outterEnvelope = Instantiate(extrusionPrefab, exteriorPolygon.transform).GetComponent<Extrusion>();
                outterEnvelope.totalHeight = floor2floor;
                outterEnvelope.Initialize(exteriorPolygon);
                exteriorPolygon.extrusion = outterEnvelope;
                Material material;
                material = Resources.Load("Materials/Envelope") as Material;
                exteriorPolygon.SetOriginalMaterial(material);

                if (OnOutterEnvelopeChanged != null)
                {
                    OnOutterEnvelopeChanged(this);
                }
            }
            else
            {
                UpdateExteriorPolygon();
            }
        }

        /// <summary>
        /// Sets the box collider of this tower floor
        /// </summary>
        /// <param name="centre">The centre of the box collider</param>
        /// <param name="size">The size of the box collider</param>
        /// <returns>Collider</returns>
        public Collider SetBoxCollider(Vector3 centre, Vector3 size)
        {
            var collider = GetComponent<BoxCollider>();
            if (collider == null)
            {
                collider = gameObject.AddComponent<BoxCollider>();
            }
            collider.enabled = true;
            collider.center = centre;
            collider.size = size;
            return collider;
        }

        /// <summary>
        /// Returns the corners of all the apartments in this tower floor except the given apartment
        /// </summary>
        /// <param name="apt">The apartment to be excepted from the calculation</param>
        /// <returns>List of Vector3</returns>
        public List<Vector3> GetApartmentsCorners(ApartmentUnity apt)
        {
            List<Vector3> points = new List<Vector3>();

            for (int i = 0; i < apartments.Count; i++)
            {
                bool notEquals = apartments[i] != apt;
                if (i == 0 && notEquals)
                {
                    points.Add(apartments[i].editableMesh[3].currentPosition);
                    points.Add(apartments[i].editableMesh[2].currentPosition);
                }
                if (i == 1 && notEquals)
                {
                    points.Add(apartments[i].editableMesh[1].currentPosition);
                    points.Add(apartments[i].editableMesh[0].currentPosition);
                }
                if (i == 2 && notEquals)
                {
                    points.Add(apartments[i].editableMesh[2].currentPosition);
                    points.Add(apartments[i].editableMesh[1].currentPosition);
                }
                if (i == 3 && notEquals)
                {
                    points.Add(apartments[i].editableMesh[3].currentPosition);
                    points.Add(apartments[i].editableMesh[2].currentPosition);
                }
                if (i == 4 && notEquals)
                {
                    points.Add(apartments[i].editableMesh[1].currentPosition);
                    points.Add(apartments[i].editableMesh[0].currentPosition);
                }
                if (i == 5 && notEquals)
                {
                    points.Add(apartments[i].editableMesh[2].currentPosition);
                    points.Add(apartments[i].editableMesh[1].currentPosition);
                }
            }
            return points;
        }

        /// <summary>
        /// Generates the exterior polygon of the floor layout (outline of the external wall)
        /// </summary>
        /// <param name="tower">Base tower to be used for the generation</param>
        public void GenerateExteriorPolygon(ProceduralTower tower)
        {
            if (tower.exteriorPolygon != null)
            {
                if (exteriorPolygon != null)
                {
                    Destroy(exteriorPolygon.gameObject);
                    exteriorPolygon = null;
                }

                exteriorPolygon = Instantiate(polygonPrefab, transform).GetComponent<Polygon>();
                exteriorPolygon.gameObject.name = "ExteriorEnvelopePolygon";
                exteriorPolygon.GetComponent<MeshRenderer>().enabled = false;
                exteriorPolygon.transform.localPosition = Vector3.zero;

                List<PolygonVertex> verts = new List<PolygonVertex>();

                for (int i = 0; i < tower.exteriorPolygon.Count; i++)
                {
                    PolygonVertex vert = Instantiate(polygonVertexPrefab, exteriorPolygon.transform).GetComponent<PolygonVertex>();
                    vert.Initialize(i);
                    vert.UpdatePosition(new Vector3(tower.exteriorPolygon[i].currentPosition.x, 0, tower.exteriorPolygon[i].currentPosition.z));
                    verts.Add(vert);
                }

                exteriorPolygon.Initialize(verts, true, true, false);
                Extrusion outterEnvelope = Instantiate(extrusionPrefab, exteriorPolygon.transform).GetComponent<Extrusion>();
                outterEnvelope.totalHeight = floor2floor;
                outterEnvelope.Initialize(exteriorPolygon);
                exteriorPolygon.extrusion = outterEnvelope;
                Material material;
                material = Resources.Load("Materials/Commercial") as Material;
                exteriorPolygon.SetOriginalMaterial(material);
            }
        }
        #endregion

        #region Private Methods
        protected override void LoadApartments(FloorLayoutState loadState)
        {
            List<ApartmentUnity> apts = new List<ApartmentUnity>();
            for (int i = 0; i < loadState.apartmentStates.Count; i++)
            {
                var aptState = loadState.apartmentStates[i];
                List<Vector3> temp_points = new List<Vector3>();
                for (int j = 0; j < aptState.vertices.Length; j += 3)
                {
                    temp_points.Add(new Vector3(aptState.vertices[j], aptState.vertices[j + 1], aptState.vertices[j + 2]));
                }

                if (aptState.isCorner)
                {
                    CreateCornerApartment("SpecialApt", temp_points.ToArray(), aptState.id, aptState.isExterior);
                }
                else
                {
                    apts.Add(CreateApartment(aptState.type, temp_points.ToArray(), aptState.id, aptState.corridorIndex, aptState.hasCorridor, "", aptState.triangles, aptState.flipped));
                }
            }

            for (int i = 0; i < apts.Count; i++)
            {
                apts[i].TowerApartment = true;
                apts[i].editableMesh.towerApt = true;
                switch (i)
                {
                    case 0:
                        apts[i].flipped = true;
                        apts[i].AlterExtensionAxes(false, true, true);
                        break;
                    case 1:
                        apts[i].AlterExtensionAxes(false, false, true);
                        break;
                    case 2:
                        apts[i].AlterExtensionAxes(true, false, true);
                        break;
                    case 3:
                        apts[i].flipped = true;
                        apts[i].AlterExtensionAxes(false, true, true);
                        break;
                    case 4:
                        apts[i].AlterExtensionAxes(false, false, true);
                        break;
                    default:
                        apts[i].flipped = true;
                        apts[i].AlterExtensionAxes(true, true, true);
                        break;
                }
                if (apts[i].ApartmentType == "SpecialApt" && !apts[i].IsCorner)
                {
                    apts[i].SetCloseOnes(apts);
                }
                apts[i].CheckNeighbours();
            }

            //for (int i = 0; i < floorVertices.Count; i++)
            //{
            //    floorVertices[i].SetGroup(floorVertices);
            //}
        }

        protected override float GetTotalFloorArea()
        {
            return base.GetTotalFloorArea();
        }
        protected override float GetNIA()
        {
            return 0;
        }
        private void OnApartmentUpdated(BaseDesignData sender)
        {
            UpdateExteriorPolygon();
        }

        private void UpdateExteriorPolygon()
        {
            if (baseTower == null)
            {
                Vector3[] points = new Vector3[]
                {
                apartments[0].editableMesh[3].currentPosition,
                apartments[0].editableMesh[2].currentPosition,
                apartments[1].editableMesh[1].currentPosition,
                apartments[1].editableMesh[0].currentPosition,
                apartments[2].editableMesh[2].currentPosition,
                apartments[2].editableMesh[1].currentPosition,
                apartments[3].editableMesh[3].currentPosition,
                apartments[3].editableMesh[2].currentPosition,
                apartments[4].editableMesh[1].currentPosition,
                apartments[4].editableMesh[0].currentPosition,
                apartments[5].editableMesh[2].currentPosition,
                apartments[5].editableMesh[1].currentPosition,
                };

                var offsetPoints = Polygon.OffsetPoints(points, true, -Standards.TaggedObject.ConstructionFeatures["ExteriorWall"]);

                for (int i = 0; i < offsetPoints.Length; i++)
                {
                    exteriorPolygon[i].SetPosition(offsetPoints[i]);
                }

                if (OnOutterEnvelopeChanged != null)
                {
                    OnOutterEnvelopeChanged(this);
                }
            }
            else
            {
                UpdateExteriorPolygon(baseTower);
            }
        }

        private void UpdateExteriorPolygon(ProceduralTower tower)
        {
            if (exteriorPolygon == null)
            {
                GenerateExteriorPolygon(tower);
            }
            else
            {
                for (int i = 0; i < tower.exteriorPolygon.Count; i++)
                {
                    exteriorPolygon[i].SetPosition(new Vector3(tower.exteriorPolygon[i].currentPosition.x, 0, tower.exteriorPolygon[i].currentPosition.z));
                }
            }

            if (building.heightLine == null)
            {
                building.AddHeightLine();
            }
            building.UpdateHeightLine(null);
        }

        private void OnBaseTowerChanged(ProceduralTower sender)
        {
            if (baseTower != null && sender == baseTower)
            {
                UpdateExteriorPolygon(sender);
            }
        }

        private void GetCore()
        {
            if (coresParent != null)
            {
                DestroyImmediate(coresParent);
            }
            var _core = building.currentCoreDimensions;
            coreDimensions = new Vector3(_core.x, floor2floor, _core.z);
            coresParent = new GameObject("CoresParent");
            coresParent.transform.SetParent(transform);
            coresParent.transform.localPosition = Vector3.zero;
            coresParent.transform.localEulerAngles = Vector3.zero;

            GameObject core = GameObject.CreatePrimitive(PrimitiveType.Cube);
            core.transform.SetParent(coresParent.transform);
            core.transform.localScale = coreDimensions;
            core.transform.localPosition = new Vector3(0, coreDimensions.y / 2.0f, 0);
            core.transform.localEulerAngles = Vector3.zero;
        }

        private void GetApartments()
        {
            if (aptParent != null)
            {
                DestroyImmediate(aptParent);
            }

            floorVertices = new List<GeometryVertex>();
            apartments = new List<ApartmentUnity>(6);
            aptParent = new GameObject("ApartmentsParent");
            aptParent.transform.SetParent(transform);
            aptParent.transform.localPosition = Vector3.zero;
            aptParent.transform.localEulerAngles = Vector3.zero;
            Vector3 right = new Vector3(1, 0, 0);
            Vector3 forward = new Vector3(0, 0, 1);
            for (int i = 0; i < aptTypesList.Length; i++)
            {
                float aptEnvelopeWidth = (float)(Standards.TaggedObject.ApartmentTypesMinimumSizes[aptTypesList[i]] / apartmentDepth) + Standards.TaggedObject.ConstructionFeatures["PartyWall"];
                float offsetDist = apartmentDepth + (Standards.TaggedObject.ConstructionFeatures["CorridorWall"]);
                Vector3[] verts = new Vector3[0];
                ApartmentUnity apt;
                switch (i)
                {
                    case 0:
                        verts = GetAptsVerts(new Vector3(0, 0, coreDimensions.z / 2), -right, forward, aptEnvelopeWidth, offsetDist);
                        apt = CreateApartment(aptTypesList[i], verts, i);
                        apt.flipped = true;
                        apt.AlterExtensionAxes(false, true, true);
                        break;
                    case 1:
                        verts = GetAptsVerts(new Vector3(0, 0, coreDimensions.z / 2), right, forward, aptEnvelopeWidth, offsetDist);
                        apt = CreateApartment(aptTypesList[i], verts, i);
                        apt.AlterExtensionAxes(false, false, true);
                        break;
                    case 2:
                        verts = GetAptsVerts(new Vector3(coreDimensions.x / 2, 0, coreDimensions.z / 2), -forward, right, coreDimensions.z, offsetDist);
                        apt = CreateApartment(aptTypesList[i], verts, i);
                        apt.AlterExtensionAxes(true, false, true);
                        break;
                    case 3:
                        verts = GetAptsVerts(new Vector3(0, 0, -coreDimensions.z / 2), right, -forward, aptEnvelopeWidth, offsetDist);
                        apt = CreateApartment(aptTypesList[i], verts, i);
                        apt.flipped = true;
                        apt.AlterExtensionAxes(false, true, true);
                        break;
                    case 4:
                        verts = GetAptsVerts(new Vector3(0, 0, -coreDimensions.z / 2), -right, -forward, aptEnvelopeWidth, offsetDist);
                        apt = CreateApartment(aptTypesList[i], verts, i);
                        apt.AlterExtensionAxes(false, false, true);
                        break;
                    default:
                        verts = GetAptsVerts(new Vector3(-coreDimensions.x / 2, 0, coreDimensions.z / 2), -forward, -right, coreDimensions.z, offsetDist);
                        apt = CreateApartment(aptTypesList[i], verts, i);
                        apt.flipped = true;
                        apt.AlterExtensionAxes(true, true, true);
                        break;
                }
                apt.editableMesh.snappingEdges = true;
                apt.editableMesh.towerApt = true;
                apt.ProceduralFloor = this;
                apt.TowerApartment = true;
                apartments.Add(apt);
            }

            for (int i = 0; i < apartments.Count; i++)
            {
                apartments[i].SetCloseOnes(apartments);
                apartments[i].CheckNeighbours();
            }

            //for (int i = 0; i < floorVertices.Count; i++)
            //{
            //    floorVertices[i].SetGroup(floorVertices);
            //}
        }

        private Vector3[] GetAptsVerts(Vector3 start, Vector3 direction1, Vector3 direction2, float width, float depth)
        {
            Vector3[] verts = new Vector3[4];

            //GameObject obj;

            verts[0] = start;
            verts[1] = start + direction2 * depth;
            verts[2] = verts[1] + direction1 * width;
            verts[3] = start + direction1 * width;
            if (Polygon.IsClockWise(verts.ToList()))
            {
                verts = verts.Reverse().ToArray();
            }
            return verts;
        }

        private ApartmentUnity CreateApartment(string type, Vector3[] verts, int i, string name = "", int[] _tris = null)
        {
            int index = aptParent.transform.childCount;
            i = index;
            GameObject apartment = Instantiate(apartmentPrefab, aptParent.transform);
            if (string.IsNullOrEmpty(name))
            {
                apartment.name = "Apartment_" + i + "_" + type + name;
            }
            else
            {
                apartment.name = name;
            }
            apartment.transform.localPosition = verts[3];
            apartment.transform.LookAt(aptParent.transform.TransformPoint(verts[2]));

            ApartmentUnity aptU = apartment.GetComponent<ApartmentUnity>();
            aptU.HasCorridor = false;
            aptU.hasExtrusion = true;
            aptU.extrusionMoveable = false;
            aptU.extrusionEditEdges = true;
            aptU.extrusionCapped = false;
            aptU.isExterior = true;
            aptU.Initialize(i, type);
            //AddApartment(aptU);

            GameObject meshObject = new GameObject("MeshObject");
            meshObject.transform.SetParent(apartment.transform);
            meshObject.transform.localPosition = Vector3.zero;
            meshObject.transform.localEulerAngles = Vector3.zero;
            meshObject.layer = 8;
            var filter = meshObject.AddComponent<MeshFilter>();
            var renderer = meshObject.AddComponent<MeshRenderer>();
            var collider = meshObject.AddComponent<MeshCollider>();

            Mesh mesh = new Mesh();
            Vector3[] meshVerts = new Vector3[verts.Length];
            Vector2[] uvs = new Vector2[verts.Length];
            for (int j = 0; j < meshVerts.Length; j++)
            {
                meshVerts[j] = meshObject.transform.InverseTransformPoint(transform.TransformPoint(verts[j]));
                uvs[j] = new Vector2(meshVerts[j].x, meshVerts[j].z);
            }
            mesh.vertices = meshVerts;
            bool flip = true;
            if (_tris == null)
            {
                if (flip)
                {
                    mesh.triangles = new int[] { 0, 2, 1, 0, 3, 2 };
                }
                else
                {
                    mesh.triangles = new int[] { 1, 2, 0, 2, 3, 0 };
                }
            }
            else
            {
                mesh.triangles = _tris;
            }
            mesh.uv = uvs;
            mesh.uv2 = uvs;
            mesh.uv3 = uvs;
            mesh.uv4 = uvs;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            filter.sharedMesh = mesh;
            collider.sharedMesh = mesh;
            renderer.material = originalMaterial;

            var editMesh = meshObject.AddComponent<EditableMesh>();
            editMesh.capped = false;
            editMesh.editEdges = true;
            editMesh.moveable = false;
            editMesh.originalMaterial = originalMaterial;
            editMesh.typeMaterial = new Material(typeMaterial);
            editMesh.SetTypeColor(Standards.TaggedObject.ApartmentTypesColours[type]);
            editMesh.Initialize(editMesh.GetComponent<MeshFilter>().sharedMesh, editMesh.transform);
            var temp_verts = editMesh.vertices.Select(x => (GeometryVertex)x).ToList();
            floorVertices.AddRange(temp_verts);
            aptU.SetEditableMesh(editMesh);
            aptU.editableMesh.TypeHighlight(false);
            editMesh.meshExtrusion.GetComponent<MeshCollider>().convex = true;
            aptU.CheckAgainstConstraints();
            return aptU;
        }
        #endregion
    }
}
