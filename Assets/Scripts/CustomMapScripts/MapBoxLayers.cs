﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Map;

public class MapBoxLayers : MonoBehaviour {

    public Camera my_camera;
    bool visualize;
	// Use this for initialization
	void Start () {
        visualize = true;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BuildingsVisualizer()
    {
        visualize = !visualize;
        if (visualize)
        {
            my_camera.cullingMask |= (1 << 13);
        }
        else
        {
            my_camera.cullingMask &= ~(1 << 13);
        }
    }
}
