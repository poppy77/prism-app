﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrydenWoodUnity.DataViz
{
    public class Plot2D : MonoBehaviour
    {
        public GameObject ptPrefab;
        public Transform ptParent;
        public Text minX;
        public Text maxX;
        public Text minY;
        public Text maxY;
        public PlotType plotType = PlotType.Scaleable;
        public int ptSize = 3;
        public Color ptColour = Color.black;
        public float xMin { get; private set; }
        public float yMin { get; private set; }
        public float xMax { get; private set; }
        public float yMax { get; private set; }

        private List<Vector2> values;
        private RectTransform rectTrans;
        private float width;
        private float height;
        private GameObject currentObj;

        // Use this for initialization
        void Awake()
        {
            rectTrans = GetComponent<RectTransform>();
            width = rectTrans.rect.width;
            height = rectTrans.rect.height;
        }

        // Update is called once per frame
        void Update()
        {
            //if (Input.GetKeyDown("m"))
            //{
            //    AddValue(new Vector2(1, 1));
            //    AddValue(new Vector2(2, 2));
            //}
        }

        public void AddValue(Vector2 value)
        {
            if (values == null)
            {
                values = new List<Vector2>();
                xMin = yMin = float.MaxValue;
                xMax = yMax = float.MinValue;
            }

            if (value.x < xMin)
            {
                xMin = value.x;
                minX.text = xMin.ToString();
            }
            if (value.y < yMin)
            {
                yMin = value.y;
                minY.text = yMin.ToString();
            }
            if (value.x > xMax)
            {
                xMax = value.x;
                maxX.text = xMax.ToString();
            }
            if (value.y > yMax)
            {
                yMax = value.y;
                maxY.text = yMax.ToString();
            }

            values.Add(value);
            currentObj = Instantiate(ptPrefab, ptParent);
            currentObj.transform.localPosition = new Vector3(width / 2, height / 2, 0);

            DrawGraph();
        }

        public void DrawGraph()
        {
            switch (plotType)
            {
                case PlotType.Scaleable:
                    DrawScaleableGraph();
                    break;
            }
        }

        private void DrawScaleableGraph()
        {
            if (ptParent.childCount > 1)
            {
                for (int i = 0; i < ptParent.childCount; i++)
                {
                    ptParent.GetChild(i).localPosition = new Vector3(BrydenWoodUtils.Remap(values[i].x, xMin, xMax, 0, width), BrydenWoodUtils.Remap(values[i].y, yMin, yMax, 0, height), 0);
                }
            }
        }
    }

    public enum PlotType
    {
        Rolling,
        Standard,
        Scaleable
    }
}
