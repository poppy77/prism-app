﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BrydenWoodUnity.GeometryManipulation
{
    /// <summary>
    /// Used when a geometry is selected
    /// </summary>
    public delegate void OnSelectGeometry(bool triggerEvent = true);

    /// <summary>
    /// Interface to be implemented by a geometry that should be selectable
    /// </summary>
    public interface ISelectableGeometry
    {
        /// <summary>
        /// Triggered when the geometry is selected
        /// </summary>
        event OnSelectGeometry selected;
        /// <summary>
        /// Triggered when the geometry is deselected
        /// </summary>
        event OnSelectGeometry deselected;
        /// <summary>
        /// Called to select the geometry
        /// </summary>
        void Select();
        /// <summary>
        /// Called to deselect the geometry
        /// </summary>
        void DeSelect();
    }
}
