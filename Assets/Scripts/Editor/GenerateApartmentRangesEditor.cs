﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BrydenWoodUnity.UIElements
{
    [CustomEditor(typeof(GenerateApartmentRanges))]
    public class GenerateApartmentRangesEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            GenerateApartmentRanges m_target = (GenerateApartmentRanges)target;
            if (GUILayout.Button("Generate Apt Types"))
            {
                m_target.GenerateApartType();
            }
        }
    }
}
